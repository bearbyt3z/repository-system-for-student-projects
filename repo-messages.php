<?php require_once 'repo-common.php'; ?>

<?php session_start(); ?>

<?php if (isset($_SESSION['errors'])): ?>
    <div class="alert alert-danger">
        <?php foreach($_SESSION['errors'] as $error): ?>
            <p><?php echo $error ?></p>
        <?php endforeach; ?>
    </div>
    <?php unset($_SESSION['errors']); ?>
<?php endif; ?>

<?php if (isset($_SESSION['messages'])): ?>
    <div class="alert alert-success">
        <?php foreach($_SESSION['messages'] as $msg): ?>
            <p><?php echo $msg ?></p>
        <?php endforeach; ?>
    </div>
    <?php unset($_SESSION['messages']); ?>
<?php endif; ?>
