<?php require_once 'repo-common.php'; ?>
<?php require_once 'lib/password.php'; ?>

<?php

session_start();

// $pattern = '(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}';
$newPassword = prepareInput($_POST['newpassword']);

$userIndex = $_SESSION['index'];
$userName = $_SESSION['username'];

if (empty($userIndex))
{
    logoutUser('Twoja sesja została zakończona. Zaloguj się ponownie.', 'errors');
}
// elseif (preg_match($pattern, $newPassword) != 1)
// {
//     $_SESSION['errors'] = array('Hasło musi mieć przynajmniej 8 znaków, w tym małą i dużą literę oraz cyfrę.');
//     redirect('repo.php?action=changepwd');
// }
elseif (!empty($newPassword))
{
    $fpDb = fopen($CONFIG['global']['DATABASE_PATH'], 'r+');
    flock($fpDb, LOCK_EX);
    opcache_invalidate($CONFIG['global']['DATABASE_PATH'], true);  // remove db file from opcache
    $db = include $CONFIG['global']['DATABASE_PATH'];
    $db['users'][$userIndex]['password'] = password_hash($newPassword, PASSWORD_DEFAULT);
    ftruncate($fpDb, 0);  // set 0 size
    $dbWriteResult = fwrite($fpDb, '<?php return ' . var_export($db, true) . '; ?>');
    fflush($fpDb);
    flock($fpDb, LOCK_UN);
    fclose($fpDb);
    $db = null;  // free db from memory
    if ($dbWriteResult)
    {
        if ($CONFIG['notifier']['PASSWORD_CHANGED'])
            sendEmail($CONFIG['global']['ADMIN_EMAIL'], "Imię i nazwisko: $userName\r\nHasło zostało zmienione.");

        $_SESSION['messages'] = array('Twoje hasło zostało zmienione.');
        redirect($CONFIG['global']['REPO_HOME_URL']);
    }
}

?>
