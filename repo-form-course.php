<?php require_once 'repo-common.php'; ?>

<?php $courselist = getCourseList(); ?>

<div class="form-group">
    <label class="col-sm-3 control-label" for="course">Wybierz przedmiot:</label>
    <div class="col-sm-5">
        <select id="course" class="form-control" required name="course" onchange="setProjectFormInputs(this, <?php echo ($checkDeadline === false) ? 'false' : 'true'; /* checkDeadline can be null */ ?>);">
            <option selected disabled hidden style="display: none" value=""></option>
            <?php foreach ($courselist as $shortname => $fullname) : ?>
                <option value="<?php echo $shortname ?>"><?php echo $fullname ?></option>
            <?php endforeach ?>
        </select>
    </div>
</div>
