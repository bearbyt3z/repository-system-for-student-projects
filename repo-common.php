<?php

// Redirection if accessed directly

function redirect($url, $statusCode = 303)
{
   header('Location: ' . $url, true, $statusCode);
   die();
}

if (!defined('MAIN_FILE_ACCESS'))
{
    // $dir = '//'.$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF']);
    redirect($CONFIG['global']['REPO_HOME_URL'], 301);
}

// END Redirection

// Configutation
$CONFIG = include 'repo-config.php';
$REPO_DIRECTORY = $CONFIG['global']['REPO_DIRECTORY'];
// END Configuration

// Create repo directory if does not exists
if (!file_exists($REPO_DIRECTORY))
{
    $oldMask = umask(0);
    if (!mkdir($REPO_DIRECTORY, 0777, true))  // will fail if . dir doesn't have write privilages for others (chmod o+w)
    {
        die('Błąd podczas tworzenia katalogu REPO...');
    }
    umask($oldMask);
}
// END Repo directory


function prepareInput($data)
{
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}

function isPastDate($string)
{
    global $CONFIG;
    date_default_timezone_set('Europe/Warsaw');
    $today = new DateTime();  // TODO: midnight parameter???
    $today->setTime(0, 0);
    return (DateTime::createFromFormat($CONFIG['global']['DATE_FORMAT'], $string) < $today);
}

function getCourseList()
{
    global $CONFIG;
    $list = array();
    foreach ($CONFIG['course'] as $key => $course)
    {
        if (empty($course['deadline']) || !isPastDate($course['deadline']))
            $list[$key] = $course['fullname'];
    }
    return $list;
}

function getProjectList($course, $checkDeadline = true)
{
    global $CONFIG;
    $list = array();
    foreach ($CONFIG['course'][$course]['projects'] as $key => $value)
    {
        // list($project, $deadline) = is_string($key) ? [$key, $value] : [$value, null];    // error 500 on PHP 5
        list($project, $deadline) = is_string($key) ? array($key, $value) : array($value, null);
        if (!$checkDeadline || empty($deadline) || !isPastDate($deadline))
            array_push($list, $project);
    }
    return json_encode($list);
}

function getAcceptedFileTypes($course)
{
    global $CONFIG;
    return json_encode($CONFIG['course'][$course]['accepted']);
}

function sendEmail($address, $message)
{
    // $address = $CONFIG['global']['ADMIN_EMAIL'];
    global $CONFIG;

    $adminName = $CONFIG['global']['ADMIN_NAME'];
    $adminName = '=?UTF-8?B?' . base64_encode($adminName) . '?=';
    $adminEmail = $CONFIG['global']['ADMIN_EMAIL'];

    $subject = 'Wiadomość z systemu ERPS';
    $subject = '=?UTF-8?B?' . base64_encode($subject) . '?=';

    $headers = 'From: ' . $adminName . ' <' . $adminEmail . '>' . "\r\n" .
                'Reply-To: ' . $adminName . ' <' . $adminEmail . '>' . "\r\n" .
                'MIME-Version: 1.0' . "\r\n" .
                'Content-Type: text/plain; charset=UTF-8' . "\r\n" .
                'X-Mailer: PHP/' . phpversion();
                // 'Content-type: text/html; charset=iso-8859-2' . "\r\n" .

    $fullMsg = 'Witaj!' . "\r\n"
            . 'Poniżej znajduje się wiadomość z systemu ERPS:' . "\r\n\r\n"
            . $message . "\r\n\r\n"
            . '--' . "\r\n"
            . 'Wiadomość została wygenerowana automatycznie.' . "\r\n"
            . 'Możesz na nią odpowiedzieć, jeśli masz jakieś pytania.';

    return mail($address, $subject, $fullMsg, $headers);
}

function printAllFiles($dir)
{
    global $REPO_DIRECTORY;
    $output = '<div class="table-responsive"><table class="table table-hover"><tbody>';  /// TODO THEAD
    $allFiles = scandir($dir);
    $internalDir = str_replace($REPO_DIRECTORY, '', $dir);
    for($i = 2; $i < count($allFiles); $i++)  // omit . and ..
    {
        $output .= '<tr><td><a href="#" title="Usuń plik" onclick="removeFile(this, \'' . $internalDir . '\', \'' . $allFiles[$i] . '\'); return false;" class="remove-icon"><span class="glyphicon glyphicon-remove"></span></a>';
        $output .= '<span class="file-name">' . $allFiles[$i] . '</span></td></tr>'; // TODO: Show file => show copy?
        // $output .= '<tr class="file-row"><td>' . $allFiles[$i] . '</td>';
        // $output .= '<td><a href="#" class="remove-icon" onmouseover="this.parentNode.parentNode.style.color = \'red\';" onmouseout="this.parentNode.parentNode.style.color = \'black\';">';
        // $output .= '<span class="glyphicon glyphicon-remove"></span></a></td></tr>';
    }
    $output .= '</tbody></table></div>';
    echo $output;
}


function removeFile($filePath)
{
    global $REPO_DIRECTORY;
    $filePath = $REPO_DIRECTORY . $filePath;
    $returnMsg = '';
    if (file_exists($filePath))
    {
        if (unlink($filePath))
            $returnMsg = 'Plik został usunięty!';
        else
            $returnMsg = 'Błąd: Nie można usunąć pliku!';
    }
    else
        $returnMsg = 'Błąd: Plik nie istnieje!';

    return $returnMsg;
}


function logoutUser($msg = 'Zostałeś poprawnie wylogowany.', $arrName = 'messages')
{
    global $CONFIG;
    session_unset();
    session_destroy();
    session_start();
    $_SESSION[$arrName] = array($msg);
    header('Location: ' . $CONFIG['global']['REPO_HOME_URL']);
    exit(0);
}


function getRandomPassword($length)
{
    // $randomPassword = bin2hex(openssl_random_pseudo_bytes($CONFIG['global']['RANDOM_PASSWORD_LENGTH'] / 2));
    $bitCount = floor($length / 2);
    $randomBits = file_get_contents('/dev/urandom', false, null, 0, $bitCount);
    $password = bin2hex($randomBits);
    return $password;
}

// function reArrayFiles(&$file_post) {
//
//     $file_ary = array();
//     $file_count = count($file_post['name']);
//     $file_keys = array_keys($file_post);
//
//     for ($i = 0; $i < $file_count; $i++) {
//         foreach ($file_keys as $key) {
//             $file_ary[$i][$key] = $file_post[$key][$i];
//         }
//     }
//
//     return $file_ary;
// }


?>
