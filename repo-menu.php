<?php require_once 'repo-common.php'; ?>

<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Otwórz nawigację</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?php echo $CONFIG['global']['REPO_HOME_URL']; ?>">System ERPS</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <!-- <li class="active"><a href="#">Dodaj pliki</a></li> -->
                <li><a href="<?php echo $CONFIG['global']['REPO_HOME_URL'] . '?action=upload'; ?>">Dodaj pliki</a></li>
                <li><a href="<?php echo $CONFIG['global']['REPO_HOME_URL'] . '?action=list'; ?>">Pokaż pliki</a></li>
                <li><a href="<?php echo $CONFIG['global']['REPO_HOME_URL'] . '?action=changepwd'; ?>">Zmień hasło</a></li>
                <!-- <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="#">Action</a></li>
                        <li><a href="#">Another action</a></li>
                        <li><a href="#">Something else here</a></li>
                        <li role="separator" class="divider"></li>
                        <li class="dropdown-header">Nav header</li>
                        <li><a href="#">Separated link</a></li>
                        <li><a href="#">One more separated link</a></li>
                    </ul>
                </li> -->
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a id="contact" href="" onmouseover="showcontact(this);" onmouseout="this.href='';">Kontakt</a></li>
                <li><a href="<?php echo $CONFIG['global']['REPO_HOME_URL'] . '?action=logout'; ?>">Wyloguj</a></li>
            </ul>
        </div>  <!-- nav-collapse -->
    </div>
</nav>

<script>
$(document).ready(function()
{
    $('li a[href$="action=<?php echo $action; ?>"]').parent().addClass('active');
});
</script>
