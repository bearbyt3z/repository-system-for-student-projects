<?php

// you have to rename this file to: repo-config.php

return array(
    'global' => array(
        'REPO_HOME_URL' => 'https://www.homeurl.of/your-repository',
        'REPO_HOME_URL_OLD_DOMAIN' => 'https://wwwold.homeurl.of/your-repository',  // other domain that is able to serve email sending (if the main is unable)
        // database and project directory should be outside of the public_html for security reasons
        'REPO_DIRECTORY' => '../../repo-dir',    // must have write privileges for others!
        'DATABASE_PATH' => '../../repo-db.dat',  // must have write privileges for others!
        'ADMIN_EMAIL' => 'username@domain.org',
        'ADMIN_NAME' => 'Your Admin Name',
        // test user for accessing the repository for testing purposes (there is no admin access etc.)
        'TEST_USER_INDEX' => '999999',
        'TEST_USER_PASSWORD' => 'DontTryToBreakIn',
        'TEST_USER_NAME' => 'Test User',
        'RANDOM_PASSWORD_LENGTH' => 8,    // should be an even number! (bin2hex)
        'STUDENT_DOMAIN' => 'students.domain.org',
        'DATE_FORMAT' => 'Y-m-d'
    ),
    'notifier' => array(  // notify admin via email about ...
        'ERRORS' => true,
        'RANDOM_PASSWORD_GENERATED' => true,
        'PASSWORD_CHANGED' => true,
        'NEW_FILES_UPLOADED' => true
    ),
    'course' => array(
        'C++' => array(
            'fullname' => 'C++ Programming',
            'projects' => array(
                'Project 1 - Class Introduction' => '2020-02-01',  // 'project name' => 'deadline date',
                'Project 2 - Inheritance' => '2019-03-01',
                'Project 3 - Interfaces' => '2020-04-01',
                // ...
                'Final Project' => '2020-06-30'  // remember to delete comma at the end if doing copy/paste ;)
            ),
            'deadline' => '2020-06-30',  // the deadline of the whole lecture (higher priority than project deadlines)
            'accepted' => '.cpp, .h, .zip, .rar, .7z'  // accepted files - only as a hint on the client side, this is not checked at the server side (yet)
        ),
        'Web' => array(
            'fullname' => 'Advanced Web Development',
            'projects' => array(
                'Project 1 - JS Introduction' => '2020-02-12',
                'Proejct 2 - Node Introduction' => '2020-03-13',
                'Proejct 3 - Express' => '2020-04-14'
            ),
            'deadline' => '2020-07-31',
            'accepted' => '.js, .zip, .rar, .7z'
        ),
        'Java' => array(
            'fullname' => 'Introduction to Java',
            'projects' => array('Project 1', 'Project 2', 'Project 3'),  // only project names (the same deadline for each project)
            'deadline' => '2020-08-23',
            'accepted' => ''  // accept everything
        )
    )
);

?>
