<?php require_once 'repo-common.php'; ?>

<footer class="footer">
    <div class="container text-right">
        <p class="text-muted">Copyright &copy; 2018-<?php echo date('Y'); ?> | <small><a href="http://www.is.umk.pl/~kdobosz">Krzysztof Dobosz</a></small></p>
    </div>
</footer>
