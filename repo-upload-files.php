<?php require_once 'repo-common.php'; ?>

<?php

session_start();

$course = prepareInput($_POST['course']);
$project = prepareInput($_POST['project']);
$files = $_FILES['files'];

$userdir = $_SESSION['userdir'];

if (($course !== null) && ($project !== null) && ($userdir !== null) && isset($_FILES['files'])) :

    $userdirpath = $REPO_DIRECTORY . '/' . $course . '/' . $userdir . '/' . $project;
    //rmdir($userdirpath);
    if (!file_exists($userdirpath))
    {
        $oldmask = umask(0);
        if (!mkdir($userdirpath, 0777, true))
        {
            die("<div class=\"alert alert-danger\">Failed to create user directory: $userdirpath</div>");  // TODO: Don't show REPO_DIR!!!
        }
        umask($oldmask);
    }

    foreach ($_FILES['files']['error'] as $key => $error)
    {
        if ($error == UPLOAD_ERR_OK)
        {
            $tmp_name = $_FILES['files']['tmp_name'][$key];
            $filename = basename($_FILES['files']['name'][$key]);  // basename() may prevent filesystem traversal attacks; further validation/sanitation of the filename may be appropriate
            if (move_uploaded_file($tmp_name, "$userdirpath/$filename"))
            {
                echo "<div class=\"alert alert-success\">Plik $filename został zapisany poprawnie.</div>";
            }
            else
            {
                echo "<div class=\"alert alert-danger\">Wystąpił błąd podczas zapisu pliku $filename.</div>";
            }
        }
    }

    if ($CONFIG['notifier']['NEW_FILES_UPLOADED'])
        sendEmail($CONFIG['global']['ADMIN_EMAIL'], "Imię i nazwisko: {$_SESSION['username']}\r\n\r\nPrzedmiot: {$CONFIG['course'][$course]['fullname']}\r\nProjekt: $project\r\n\r\nZuploadowane pliki:\r\n" . implode("\r\n", $_FILES['files']['name']));

    include('repo-print-all-files.php');

endif;
?>
