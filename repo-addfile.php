<?php require_once 'repo-common.php'; ?>

<!-- <p>Wypełnij poprawnie poniższe pola formularza:</p> -->
<form class="form-horizontal" method="post" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']);?>" enctype="multipart/form-data">

    <?php
        require 'repo-form-course.php';
        require 'repo-form-project.php';
    ?>
    <fieldset class="form-group files-group" disabled>
        <label class="col-sm-3 control-label" for="files">Dodaj pliki:</label>
        <div class="col-sm-5" style="margin-bottom: 5px;">
            <div class="input-group">
                <label class="input-group-btn">
                    <span class="btn btn-primary">
                        <!-- input type file has multiple property -->
                        Przeglądaj&hellip; <input type="file" id="files" name="files[]" class="form-control" style="display: none;" onchange='addNewFile(this);'>
                    </span>
                </label>
                <div class="form-control file-info"></div>
            </div>
        </div>
    </fieldset>

    <div class="form-group">
        <div class="col-sm-offset-3 col-sm-5">
            <input type="hidden" name="action" value="upload">
            <button type="submit" class="btn btn-primary pull-right">Prześlij pliki</button>
        </div>
    </div>
</form>
