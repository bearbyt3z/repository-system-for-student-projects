<?php header('Content-type: text/javascript'); ?>

<?php define('MAIN_FILE_ACCESS', true); ?>  // allow be accessed as separate file

<?php require_once 'repo-common.php'; ?>

<?php $courselist = getCourseList(); ?>

function setProjectFormInputs(select, checkDeadline = true)
{
    let selectvalue = select.value || select.options[select.selectedIndex].value;
    setProjectOptions(selectvalue, checkDeadline);
    let projectselect = document.getElementById('project');
    projectselect.disabled = false;

    setAcceptedFileTypes(select);
}

function setProjectOptions(course, checkDeadline = true)
{
    let options = [];
    switch (course)
    {
        <?php foreach ($courselist as $shortname => $fullname) : ?>
        case '<?php echo $shortname ?>':
            options = checkDeadline ? <?php echo getProjectList($shortname, true); ?> : <?php echo getProjectList($shortname, false); ?>;
            break;
        <?php endforeach ?>
        default:
            break;
    }

    let projectselect = document.getElementById('project');

    // let oldoptions = projectselect.querySelectorAll('option');
    let oldoptions = projectselect.querySelectorAll('option:not([disabled])');
    oldoptions.forEach(function(opt)  // remove old options
    {
        opt.parentNode.removeChild(opt);
    });

    if (projectselect.querySelector('option[disabled]') == null)  // add new blank option if not present
    {
        let newblankopt = document.createElement('OPTION');
        newblankopt.value = '';
        newblankopt.selected = true;
        newblankopt.disabled = true;
        newblankopt.hidden = true;
        newblankopt.style.display = 'none';
        projectselect.appendChild(newblankopt);
    }

    options.forEach(function(opt)  // add new options
    {
        let newopt = document.createElement('OPTION');
        newopt.value = opt;
        newopt.appendChild(document.createTextNode(opt));
        projectselect.appendChild(newopt);
    });
}

function setAcceptedFileTypes(select)
{
    let selectvalue = select.value || select.options[select.selectedIndex].value;
    let accept;
    switch (selectvalue)
    {
        <?php foreach ($courselist as $shortname => $fullname) : ?>
        case '<?php echo $shortname ?>':
            accept = <?php echo getAcceptedFileTypes($shortname); ?>;
            break;
        <?php endforeach ?>
        default:
            accept = '';
    }

    let fileinputs = select.form.querySelectorAll('input[type="file"]');
    for (let f of fileinputs)
        f.accept = accept;

    // select.form.getElementsByClassName('files-group')[0].disabled = false;
}

function enableFileInputs(select)
{
    select.form.getElementsByClassName('files-group')[0].disabled = false;
}

function addNewFile(fileinput)
{
    //$("#file-info").html($(this).val());
    let fileinfo = fileinput.parentNode.parentNode.parentNode.getElementsByClassName('file-info')[0];
    fileinfo.innerHTML = fileinput.value.replace(/.*[\/\\]/, '');

    let filesgroup = fileinput.form.getElementsByClassName('files-group')[0]

    let newfileinputhtml = '<div class="input-group"><label class="input-group-btn"><span class="btn btn-primary">Przeglądaj&hellip; <input type="file" name="files[]" class="form-control" style="display: none;" onchange="addNewFile(this);"></span></label><span class="form-control file-info"></span></div>';
    let newdivgroup = document.createElement('DIV');
    newdivgroup.classList.add('col-sm-offset-3');
    newdivgroup.classList.add('col-sm-5');
    newdivgroup.style.marginBottom = "5px";
    newdivgroup.innerHTML = newfileinputhtml;
    filesgroup.appendChild(newdivgroup);

    //setAcceptedFileTypes();  TODO!!!
}

function removeFile(link, dir, file)
{
    if (confirm('Czy na pewno chcesz usunąć następujący plik?\n' + file))
    {
        $.ajax(
        {
            method : 'POST',
            url : self.location,
            // url : 'repo-remove-file.php',
            data : { action : 'remove', filepath : dir + '/' + file }
        })
        .done(function(returnmsg)
        {
            alert(returnmsg);
            if (returnmsg.indexOf('Błąd') >= 0)
                alert(returnmsg);
            else
            {
                link.closest('tr').remove();
                // let filerow = link.parentNode.parentNode;
                // filerow.parentNode.removeChild(filerow);
            }
        });
    }
}

function showcontact(element)
{
    let x = '\u0040';
    let eml = '\u006C\u0070\u002E\u006B\u006D\u0075\u002E\u0073\u0069' + x + '\u007A\u0073\u006F\u0062\u006F\u0064\u006B';
    let subject = '?Subject=System%20ERPS';
    // let contact = document.getElementById('contact');
    // contact.href = 'mailto:' + eml.split('').reverse().join('') + subject;
    element.href = 'mailto:' + eml.split('').reverse().join('') + subject;
}

function validatePassword(form)
{
    alert(form); return false;
    let passwordinputs = form.querySelector('input[type="password"]');
    if ((passwordinputs.length < 2) || (passwordinputs[0].value !== passwordinputs[1].value))
        return false;
}

function showPassword(checkbox)
{
    let form = checkbox.form;
    let oldtype, newtype;
    if (checkbox.checked)
    {
        oldtype = 'password';
        newtype = 'text';
    }
    else
    {
        oldtype = 'text';
        newtype = 'password';
    }
    let passwordinputs = form.querySelectorAll('input[type="' + oldtype + '"]');
    for (let input of passwordinputs)
        input.type = newtype;
}
