<?php require_once 'repo-common.php'; ?>

<?php
// exec i system zablokowane?
// $minindex = exec("cut -f 1 -d : /etc/passwd | grep ^[0-9]\+$ | sort -n | head -1");  // mozna by jeszcze dodac grupe 1004
// $maxindex = exec("cut -f 1 -d : /etc/passwd | grep ^[0-9]\+$ | sort -rn | head -1");
//echo "<input type=\"number\" min=\"$minindex\" max=\"$maxindex\" step=\"1\" placeholder=\"Numer indeksu\" name=\"index\">";
?>

<div id="loginbox" class="col-xs-12 col-sm-offset-1 col-sm-9 col-md-7 col-lg-6">
    <div class="panel panel-primary">
        <div class="panel-heading">Logowanie</div>
        <div class="panel-body">
            <form class="form-horizontal" role="form" method="post" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>">
                <div class="form-group">
                    <label class="col-sm-4 control-label" for="index">Numer indeksu:</label>
                    <div class="col-sm-7">
                        <input type="number" class="form-control" id="index" name="index" autofocus required min="100000" max="999999" step="1" placeholder="Wpisz numer indeksu" title="Wpisz swój sześciocyfrowy numer indeksu">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label" for="password">Hasło:</label>
                    <div class="col-sm-7">
                        <input type="password" class="form-control" id="password" name="password" required placeholder="Wpisz hasło lub imię i nazwisko" title="Jeśli nie masz jeszcze hasła, wpisz imię i nazwisko (ze spacją i polskimi znakami)">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-4 col-sm-7">
                        <div class="checkbox">
                            <label><input type="checkbox" id="showpassword" value="" onchange="showPassword(this)">Pokaż znaki</label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-4 col-sm-7">
                        <!-- <input type="hidden" name="action" value="login"> -->
                        <button type="submit" class="btn btn-primary pull-right" name="action" value="login" id="login-button">Zaloguj</button>
                        <button type="submit" class="btn btn-default pull-right" name="action" value="reset" title="Aby zresetować hasło wpisz numer indeksu oraz imię i nazwisko (ze spacją i polskimi znakami)">Resetuj</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
