<?php require_once 'repo-common.php'; ?>

<div id="passwordbox" class="col-xs-12 col-sm-offset-1 col-sm-9 col-md-7 col-lg-6">
    <div class="panel panel-primary">
        <div class="panel-heading">Zmiana hasła</div>
        <div class="panel-body">
            <form class="form-horizontal" role="form" method="post" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>">
                <div class="form-group">
                    <label class="col-sm-4 control-label" for="newpassword">Nowe hasło:</label>
                    <div class="col-sm-7">
                        <input type="password" class="form-control" id="newpassword" name="newpassword" autofocus required pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" onkeyup="checkPasswordMatch(this.form);" title="Hasło musi mieć przynajmniej 8 znaków, w tym małą i dużą literę oraz cyfrę.">
                    </div>
                </div>
                <div class="form-group has-feedback">
                    <label class="col-sm-4 control-label" for="confirmpassword">Powtórz hasło:</label>
                    <div class="col-sm-7">
                        <input type="password" class="form-control" id="confirmpassword" name="confirmpassword" required onkeyup="checkPasswordMatch(this.form);">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-4 col-sm-7">
                        <div class="checkbox">
                            <label><input type="checkbox" id="showpassword" value="" onchange="showPassword(this);">Pokaż znaki</label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-4 col-sm-7">
                        <input type="hidden" name="action" value="changepwd">
                        <button type="submit" class="btn btn-primary pull-right">Potwierdź</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script>

//pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters"
var passwordPattern = new RegExp('');

function checkPasswordMatch(form)
{
    let passwords = form.querySelectorAll('input[type="password"]');//$('input[type="password"]');
    if ($(passwords[0]).is(':invalid'))
    {
        $(passwords[0]).closest('.form-group').removeClass('has-success');
        $(passwords[0]).closest('.form-group').addClass('has-error');
    }
    else
    {
        $(passwords[0]).closest('.form-group').removeClass('has-error');
        $(passwords[0]).closest('.form-group').addClass('has-success');
    }

    if (passwords.length < 2) return false;

    if (passwords[0].value !== passwords[1].value)
    {
        $(passwords[1]).closest('.form-group').removeClass('has-success');
        $(passwords[1]).closest('.form-group').addClass('has-error');
    }
    else
    {
        $(passwords[1]).closest('.form-group').removeClass('has-error');
        $(passwords[1]).closest('.form-group').addClass('has-success');
    }
}
</script>

<style>
/*input:invalid
{
     border: 2px solid #ff0000;
}*/
</style>
