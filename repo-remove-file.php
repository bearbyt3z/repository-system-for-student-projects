<?php require_once 'repo-common.php'; ?>

<?php

$filepath = prepareInput($_POST['filepath']);

session_start();

$index = $_SESSION['index'];
$userdir = $_SESSION['userdir'];

// if user logged in and filepath inside userdir
if (!empty($index) && !empty($userdir) && !empty($filepath) && (strpos($filepath, $userdir) !== false))
{
    $msg = removeFile($filepath);
}
else
{
    $msg = 'Błąd: Nie masz dostępu do pliku!';
}

die($msg);

?>
