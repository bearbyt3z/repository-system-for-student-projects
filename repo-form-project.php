<?php require_once 'repo-common.php'; ?>

<div class="form-group">
    <label class="col-sm-3 control-label" for="project">Wybierz projekt:</label>
    <div class="col-sm-5">
        <select id="project" class="form-control" required name="project" onchange="enableFileInputs(this);">
            <option selected disabled hidden style="display: none" value=""></option>
        </select>
    </div>
</div>
