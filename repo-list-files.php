<?php require_once 'repo-common.php'; ?>

<?php

session_start();

$courselist = getCourseList();

$course = prepareInput($_POST['course']);
$project = prepareInput($_POST['project']);

$userdir = $_SESSION['userdir'];

if (empty($course) || empty($project)) :
?>

<form class="form-horizontal" method="post" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']);?>" enctype="multipart/form-data">

    <?php
        $checkDeadline = false;
        require 'repo-form-course.php';
        require 'repo-form-project.php';
    ?>

    <div class="form-group">
        <div class="col-sm-offset-3 col-sm-5">
            <input type="hidden" name="action" value="list">
            <button type="submit" class="btn btn-primary pull-right">Pokaż pliki</button>
        </div>
    </div>
</form>

<?php
else:
    $userdirpath = $REPO_DIRECTORY . '/' . $course . '/' . $userdir . '/' . $project;

    include('repo-print-all-files.php');

endif;
?>
