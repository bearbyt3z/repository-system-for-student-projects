<?php require_once 'repo-common.php'; ?>
<?php require_once 'lib/password.php'; ?>

<?php

$userIndex = prepareInput($_POST['index']);
$password = prepareInput($_POST['password']);
$action = prepareInput($_REQUEST['action']);

if (($userIndex !== null) && ($password !== null))// && (count($name) > 1))
{
    $validCredentials = false;
    $firstTimeUser = false;

    $db = include $CONFIG['global']['DATABASE_PATH'];

    if (($userIndex === $CONFIG['global']['TEST_USER_INDEX']) && ($password === $CONFIG['global']['TEST_USER_PASSWORD']))
    {
        $db['users'][$userIndex]['username'] = $CONFIG['global']['TEST_USER_NAME'];
        $db['users'][$userIndex]['userdir'] = $userIndex . ' ' . $db['users'][$userIndex]['username'];
        $validCredentials = true;
    }
    elseif (($action !== 'reset') && isset($db['users'][$userIndex]) && isset($db['users'][$userIndex]['password']) && ($db['users'][$userIndex]['password'] !== ''))
    {
        $hash = $db['users'][$userIndex]['password'];
        if (password_verify($password, $hash))
            $validCredentials = true;
    }
    else
    {
        $name = explode(' ', prepareInput($_POST['password']));
        $userName;

        $fp = fopen('/etc/passwd', 'r');
        if ($fp)
        {
            flock($fp, LOCK_SH);
            while (($buffer = fgets($fp)) !== false)
            {
                if (strpos($buffer, $userIndex) !== false)
                {
                    list(, , , , $userName) = explode(':', $buffer);
                    foreach ($name as $n)
                        if (stripos($userName, $n) === false)
                            break 2;  // some part of the name doesn't match => break while => valid = false

                    $validCredentials = true;
                    $firstTimeUser = true;

                    break;
                }
            }
            flock($fp, LOCK_UN);
            fclose($fp);
        }
    }

    session_start();

    if (!$validCredentials)
    {
        $_SESSION['errors'] = array('Wprowadziłeś niepoprawne dane!');
        // header('Location: ' . $_SERVER['PHP_SELF']);
        header('Location: ' . $CONFIG['global']['REPO_HOME_URL']);
        exit();
    }

    if ($firstTimeUser)
    {
        // include 'repo-change-password-form';
        $userMail = $userIndex . '@' . $CONFIG['global']['STUDENT_DOMAIN'];
        $randomPassword = getRandomPassword($CONFIG['global']['RANDOM_PASSWORD_LENGTH']);
        if (!sendEmail($userMail, "Imię i nazwisko: $userName\r\nTwoje hasło do logowania: $randomPassword"))
        {
            $_SESSION['errors'] = array('Wystąpił problem z wysłaniem wiadomości email.<br>Spróbuj ponownie później lub przez starą domenę: <a href="' . $CONFIG['global']['REPO_HOME_URL_OLD_DOMAIN'] . '">' . $CONFIG['global']['REPO_HOME_URL_OLD_DOMAIN'] . '</a>.');
            header('Location: ' . $CONFIG['global']['REPO_HOME_URL']);
            exit(1);
        }
        if ($CONFIG['notifier']['RANDOM_PASSWORD_GENERATED'])
            sendEmail($CONFIG['global']['ADMIN_EMAIL'], "Imię i nazwisko: $userName\r\nWygenerowane hasło do logowania: $randomPassword");

        $fpDb = fopen($CONFIG['global']['DATABASE_PATH'], 'r+');
        flock($fpDb, LOCK_EX);
        opcache_invalidate($CONFIG['global']['DATABASE_PATH'], true);  // remove db file from opcache
        $db = include $CONFIG['global']['DATABASE_PATH'];  // load db again (now it's locked)

        $db['users'][$userIndex]['password'] = password_hash($randomPassword, PASSWORD_DEFAULT);
        $db['users'][$userIndex]['username'] = $userName;
        $db['users'][$userIndex]['usermail'] = $userMail;
        $db['users'][$userIndex]['userdir'] = $userIndex . ' ' . $userName;
        ftruncate($fpDb, 0);  // set 0 size
        $dbWriteResult = fwrite($fpDb, '<?php return ' . var_export($db, true) . '; ?>');
        /* $dbWriteResult = file_put_contents($CONFIG['global']['DATABASE_PATH'], '<?php return ' . var_export($db, true) . '; ?>'); */
        fflush($fpDb);
        flock($fpDb, LOCK_UN);
        fclose($fpDb);

        if ($dbWriteResult)
        {
            logoutUser('Hasło do logowania zostało przesłane na Twój adres email.');
        }
        else
        {
            if ($CONFIG['notifier']['ERRORS'])
                sendEmail($CONFIG['global']['ADMIN_EMAIL'], "Błąd zapisu w bazie danych!!!\r\nSprawdź, czy baza ma prawo zapisu dla pozostałych (chmod o+w).\r\n\r\nNie udało się zapisać następujących danych:\r\nImię i nazwisko: $userName\r\nWygenerowane hasło do logowania: $randomPassword");
            logoutUser('Błąd zapisu w bazie danych. Skontakuj się z administratorem.', 'errors');
        }
    }

    // if it's a valid user:

    $_SESSION['index'] = $userIndex;
    $_SESSION['username'] = $db['users'][$userIndex]['username'];
    $_SESSION['userdir'] = $db['users'][$userIndex]['userdir'];

    $db = null;  // free db from memory

    // echo 'END-LOGIN';

    include 'repo-addfile.php';

}

?>
