<?php

// try {

// ini_set('display_startup_errors',1);
// ini_set('display_errors',1);
// error_reporting(-1);

header('Content-type: text/html; charset=utf8');  // for die messages

define('MAIN_FILE_ACCESS', TRUE);

require_once 'repo-common.php';  // must be after define MAIN_FILE_ACCESS !!!

//sendEmail('kdobosz@is.umk.pl', 'TEST');
// if (mail('kdobosz@is.umk.pl', 'TEST', '$fullmsg', 'From: kdobosz@is.umk.pl'))
//     echo 'SEND';
// else
//     echo 'ERROR';
// die('END');

$action = prepareInput($_REQUEST['action']);

if ($_SERVER['REQUEST_METHOD'] == 'POST')
{
    if ($action === 'remove')
    {
        require 'repo-remove-file.php';
    }
}

?>

<?php require 'repo-top.php' ?>

<body>

<div class="container">
    <h1 class="top-title">Elektronicze Repozytorium Projektów Studenckich</h1>

<?php

session_start();
require 'repo-messages.php';
// var_dump($_SESSION);

if (!isset($_SESSION['index']) || empty($_SESSION['index']) || !isset($_SESSION['username']) || empty($_SESSION['username']))
{
    if (($action === 'login') || ($action === 'reset'))
    {
        require 'repo-menu.php';
        require 'repo-login-process.php';
    }
    else
    {
        require 'repo-login-form.php';
    }
}

else //if ($_SERVER['REQUEST_METHOD'] == 'POST')
{

    require 'repo-menu.php';

    if ($action === 'upload')
    {
        if (isset($_FILES['files']))
        {
            require 'repo-upload-files.php';
        }
        else
        {
            require 'repo-addfile.php';
        }
    }
    elseif ($action === 'list')
    {
        require 'repo-list-files.php';
    }
    elseif ($action === 'changepwd')
    {
        if (isset($_POST['newpassword']))
        {
            require 'repo-change-password-process.php';
        }
        else
        {
            require 'repo-change-password-form.php';
        }
    }
    elseif ($action === 'logout')
    {
        logoutUser();
        redirect($CONFIG['global']['REPO_HOME_URL']);  // musi byc???
    }
    else
    {
        require 'repo-home-page.php';
    }

}
// else // other than POST access
// {
// }

// }
// catch (Exception $e)
// {
//     echo 'Message: ' .$e->getMessage();
// }

?>

</div>  <!-- container -->

<?php include('repo-footer.php'); ?>

</body>
</html>
